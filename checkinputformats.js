export default class CheckInputFormats {

    static notAnObject = "expected Object as input"
    static notAfunction = "expected function as an input"


    static checkInputFunction(cd) {
        if (typeof cd == 'function') {
            return cd
        }
        throw this.notAfunction
    }
    
    static checkInput(varible) {
        if (typeof varible == 'object') {
            if (!Array.isArray(varible)) {
                return varible
            }
            throw this.notAnObject
        }
        throw this.notAnObject
    }

}

