import CheckInputFormats from "../checkinputformats.js"

export default function getValues(objects) {
    try {
        objects = CheckInputFormats.checkInput(objects)
        let valueArray = []
        for (let key in objects) {
            valueArray.push(objects[key])
        }
        return valueArray
    } catch (error) {
        console.log(error)
    }
}