
import CheckInputFormats from "../checkinputformats.js"

export default function defaults(object, defaultObjects) {

    try {
        object = CheckInputFormats.checkInput(object)
        defaultObjects = CheckInputFormats.checkInput(defaultObjects)

        for (let key in defaultObjects) {
            if (!object[key]) {
                object[key] = defaultObjects[key]
            }
        }
        return object
    }
    catch (error) {
        console.log(error)
    }


}
