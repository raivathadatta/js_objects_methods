//  // Returns a copy of the object where the keys have become the values and the values the keys.

import CheckInputFormats from "../checkinputformats.js"

export default function invert(object) {
    try {
        object = CheckInputFormats.checkInput(object)
        let newObject = {}
        for (let key in object) {
            let temVlalue;
            if (typeof object[key] == 'number') {
                temVlalue = String(object[key])
            } else {
                temVlalue = object[key]
            }

            newObject[temVlalue] = key
        }
        return newObject
    } catch (error) {
        console.log(error)
    }
}