import CheckInputFormats from "../checkinputformats.js"

export default function getKeys(objects) {
    try {
        objects = CheckInputFormats.checkInput(objects)
        let keyArray = []
        for (let object in objects) {
            keyArray.push(object)
        }

        return keyArray
    } catch (error) {
        console.log(error)
    }
}