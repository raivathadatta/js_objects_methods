import CheckInputFormats from "../checkinputformats.js"

export default function getPairs(object) {

    try {
        object = CheckInputFormats.checkInput(object)
        let pairsArray = []
        for (let key in object) {
            pairsArray.push([[key, object[key]]])
        }
        console.log(pairsArray)
        return pairsArray
    } catch (error) {
        console.log(error)
    }
}