import CheckInputFormats from "../checkinputformats.js"

export default function map(object, cd) {
    try {
        object = CheckInputFormats.checkInput(object)
        cd = CheckInputFormats.checkInputFunction(cd)
        let newObject = {}
        for (let key in object) {

            newObject[key] = cd(object[key])
        }
        return newObject
    } catch (error) {
        console.log(error)
    }
}